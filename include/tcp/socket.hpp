#pragma once

#include <isocket.hpp>

#include <netinet/in.h>


namespace tcp
{
    class Socket : public ISocket
    {
        private:
            int rootFd_;
            int highFd_;
            fd_set setFd_;

            struct sockaddr_in address_;

            int maxConnections_;

            void setNonBlocking_(const int fd);

        public:
            explicit Socket();
            ~Socket();

            void setOpt(const int level, const int options) override;
            void setNonBlocking(void) override;
            void setMaxConnections(const int maxConnections) override;

            void bind(const std::string& addr, const int port) override;
            void listen(void) override;
            int close(const int fd) override;
            int accept(void) override;

            std::string recv(const int fd, const int flags) override;
            ssize_t send(const int fd, const std::string& str, const int flags) override;

            bool checkNewConnection(void) override;

            int monitorFds(struct timeval& timeout)  override;
            void clearMonitoringFds(void) override;
            void addMonitoringFd(const int fd)  override;
            void removeMonitoringFd(const int fd)  override;
            bool isMonitoringFdReady(const int fd)  override;
    };
};
