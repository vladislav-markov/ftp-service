#pragma once

namespace ftp
{
    class Connection
    {
        private:
            int fd_;

        public:
            explicit Connection(int fd);
            ~Connection();

            int getFD() const noexcept;
    };
};
