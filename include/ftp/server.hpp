#pragma once

#include <map>
#include <string>
#include <memory>

#include <isocket.hpp>
#include <ftp/connection.hpp>


namespace ftp
{
    class Server
    {
        private:
            std::shared_ptr<ISocket>       socket_;
            std::map<int, ftp::Connection> connections_;

        public:
            explicit Server(std::shared_ptr<ISocket> socket,
                            const std::string& addr = "127.0.0.1",
                            const int port = 21);
            ~Server();

            void run(void);

            int getConnectionNumbers(void) const;
            void addConnection(Connection& connection);
            void removeConnection(const Connection& connection);
            void processConnection(const Connection& connection);

            void sendHelpMsg(const Connection& connection);
            void sendFile(const Connection& connection);
    };
};
