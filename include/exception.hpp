#pragma once

#include <string>
#include <exception>


class Exception : public std::exception
{
    private:
        int         code_;
        std::string error_;

    public:
        explicit Exception(const std::string& error, const int code = 0);
        ~Exception() = default;

        virtual const char* what() const noexcept override;
        int getCode() const noexcept;
};
