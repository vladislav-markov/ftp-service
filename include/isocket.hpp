#pragma once

#include <string>

#include <sys/time.h>


// This is an interface class for socket implementation
class ISocket
{
    public:
        ISocket() = default;
        virtual ~ISocket() = default;

        virtual void setOpt(const int level, const int options) = 0;
        virtual void setNonBlocking(void)  = 0;
        virtual void setMaxConnections(const int maxConnections) = 0;

        virtual void bind(const std::string& addr, const int port) = 0;
        virtual void listen(void) = 0;
        virtual int close(const int fd) = 0;
        virtual int accept(void)  = 0;

        virtual std::string recv(const int fd, const int flags = 0)  = 0;
        virtual ssize_t send(const int fd, const std::string& str, const int flags = 0) = 0;

        virtual bool checkNewConnection(void) = 0;

        virtual int monitorFds(struct timeval& timeout) = 0;
        virtual void clearMonitoringFds(void) = 0;
        virtual void addMonitoringFd(const int fd)  = 0;
        virtual void removeMonitoringFd(const int fd)  = 0;
        virtual bool isMonitoringFdReady(const int fd) = 0;
};
