[![pipeline status](https://gitlab.com/vladislav-markov/ftp-service/badges/master/pipeline.svg)](https://gitlab.com/vladislav-markov/ftp-service/commits/master)
[![coverage report](https://gitlab.com/vladislav-markov/ftp-service/badges/master/coverage.svg)](https://gitlab.com/vladislav-markov/ftp-service/commits/master)

A lightweight ftp client and server service with console and gui interface.