cmake_minimum_required(VERSION 3.5.1)

set(CMAKE_CXX_STANDARD 14) 
set(CMAKE_CXX_STANDARD_REQUIRED on)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")

project(module-tcp)

add_library(module-tcp STATIC socket.cpp)
