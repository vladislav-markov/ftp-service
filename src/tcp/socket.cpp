#include <tcp/socket.hpp>

#include <exception.hpp>

#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>


tcp::Socket::Socket()
{
    // Create a socket file descriptor 
    this->rootFd_ = ::socket(AF_INET, SOCK_STREAM, 0);
    if (!this->rootFd_)
        throw Exception("Can't ::socket()");

    this->highFd_ = this->rootFd_;
}

tcp::Socket::~Socket()
{
    try
    {
        this->close(this->rootFd_);
    }
    catch (...)
    {
        ; // Do not propagate exception
    }
}

void tcp::Socket::setOpt(const int level, const int options)
{
    const auto opt = 1; 
    if (::setsockopt(this->rootFd_, level, options, &opt, sizeof(opt))) 
        throw Exception("Can't ::setsockopt()");
}

void tcp::Socket::setNonBlocking(void)
{
    this->setNonBlocking_(this->rootFd_);
}

void tcp::Socket::setMaxConnections(const int maxConnections)
{
    this->maxConnections_ = maxConnections;
}

void tcp::Socket::bind(const std::string& addr, const int port)
{
    this->address_.sin_family      = AF_INET;
    this->address_.sin_addr.s_addr = ::inet_addr(addr.c_str());
    this->address_.sin_port        = ::htons(port);

    if (::bind(this->rootFd_,
               reinterpret_cast<struct sockaddr *>(&this->address_),
               sizeof(this->address_)) < 0)
    {
        throw Exception("Can't ::bind()");
    }
}

void tcp::Socket::listen(void)
{
    if (::listen(this->rootFd_, this->maxConnections_) < 0) 
        throw Exception("Can't ::listen()");
}

int tcp::Socket::close(const int fd)
{
    return ::close(fd);
}

std::string tcp::Socket::recv(const int fd, const int flags)
{
    char buffer[1024] = { 0 };

    // Receive a message from a socket file descriptor
    auto bytes = ::recv(fd, buffer, sizeof(buffer), flags);
    if (bytes > 0)
        return std::string(buffer, bytes - 2);
    else
        return std::string();
}

ssize_t tcp::Socket::send(const int fd, const std::string& str, const int flags)
{
    // Send a message on a socket file descriptor
    return ::send(fd, str.c_str(), str.size(), flags);
}

int tcp::Socket::accept(void)
{
    auto addrLen = sizeof(this->address_);

    // Accept a connection on a socket file descriptor
    auto newFd = ::accept(this->rootFd_,
                          reinterpret_cast<struct sockaddr *>(&this->address_),
                          reinterpret_cast<socklen_t*>(&addrLen));
    if (newFd < 0)
        throw Exception("Can't ::accept()");

    this->setNonBlocking_(newFd);

    return newFd;
}

bool tcp::Socket::checkNewConnection(void)
{
    return this->isMonitoringFdReady(this->rootFd_);
}

int tcp::Socket::monitorFds(struct timeval& timeout)
{
    /*
     * Allow to monitor multiple file descriptors,
     * waiting until one or more of the file descriptors
     * become "ready" for some class of I/O operation
     */
    auto fds = ::select(this->highFd_ + 1,
                        &(this->setFd_),
                        reinterpret_cast<fd_set*>(0),
                        reinterpret_cast<fd_set*>(0),
                        &timeout);
    if (fds == -1)
        throw Exception("Can't ::select()");

    return fds;
}

void tcp::Socket::clearMonitoringFds(void)
{
    this->highFd_ = this->rootFd_;

    FD_ZERO(&(this->setFd_));
    FD_SET(this->rootFd_, &(this->setFd_));
}

void tcp::Socket::addMonitoringFd(const int fd)
{
    if (fd != 0)
    {
        // Add the socket file descriptor to the monitoring for select()
        FD_SET(fd, &(this->setFd_));

        // We need the highest socket for select
        if (fd > this->highFd_)
            this->highFd_ = fd;
    }
}

void tcp::Socket::removeMonitoringFd(const int fd)
{
    if (fd != 0)
        FD_CLR(fd, &(this->setFd_));
}

bool tcp::Socket::isMonitoringFdReady(const int fd)
{
    return FD_ISSET(fd, &(this->setFd_));
}

void tcp::Socket::setNonBlocking_(const int fd)
{
    auto opts = ::fcntl(fd, F_GETFL, 0);
    if (opts < 0)
        throw Exception("Can't get socket flags");

    opts = (opts | O_NONBLOCK);

    if (::fcntl(fd, F_SETFL, opts) < 0)
        throw Exception("Can't set socket to non-blocking state");
}

