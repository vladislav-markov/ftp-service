#include <ftp/server.hpp>
#include <ftp/commands.hpp>
#include <tcp/socket.hpp>

#include <exception.hpp>

#include <iostream>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


ftp::Server::Server(std::shared_ptr<ISocket> socket, const std::string& addr, const int port) : socket_(socket)
{
    try
    {
        this->socket_->setOpt(SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT);
        this->socket_->setNonBlocking();
        this->socket_->setMaxConnections(10);
        this->socket_->bind(addr, port);
    }
    catch (Exception& exception)
    {
        throw Exception("Can't run FTP-server because, " + std::string(exception.what()));
    }
    catch (...)
    {
        throw Exception("Can't run FTP-server because, caught unhandling exception!");
    }
}

ftp::Server::~Server()
{
    ;
}

void ftp::Server::run(void)
{
    this->socket_->listen();

    while (true)
    {
        this->socket_->clearMonitoringFds();

        for (auto i: this->connections_)
            this->socket_->addMonitoringFd(i.first);

        struct timeval timeout = {
            .tv_sec  = 60,
            .tv_usec = 0
        };

        this->socket_->monitorFds(timeout);

        if (this->socket_->checkNewConnection())
        {
            auto newConnection = Connection(this->socket_->accept());
            this->addConnection(newConnection);
        }

        for (auto i: this->connections_)
            this->processConnection(i.second);
    }
}

void ftp::Server::addConnection(Connection& connection)
{
    this->connections_.insert({connection.getFD(), connection});
}

void ftp::Server::removeConnection(const Connection& connection)
{
    const auto fd = connection.getFD();

    if (this->connections_.erase(fd))
        this->socket_->close(fd);
}

int ftp::Server::getConnectionNumbers(void) const
{
    return this->connections_.size();
}

void ftp::Server::processConnection(const Connection& connection)
{
    if (!this->socket_->isMonitoringFdReady(connection.getFD()))
        return;

    auto buffer = this->socket_->recv(connection.getFD());
    if (buffer.size() > 0)
    {
        if (buffer.compare(std::string("HELP")) == 0)
            this->sendHelpMsg(connection);
        else if (buffer.compare(std::string("RETR")) == 0)
            this->sendFile(connection);
        else if (buffer.compare(std::string("QUIT")) == 0)
            this->removeConnection(connection);
    }
}

void ftp::Server::sendHelpMsg(const Connection& connection)
{
    std::string helpMsg("AVILABLE COMMNADS:\nHELP\nQUIT\nRETR\n");
    this->socket_->send(connection.getFD(), helpMsg);
}

void ftp::Server::sendFile(const Connection& connection)
{
    int fileFd;
    struct stat stat_buf;

    if ((::access("test.txt", R_OK) == 0) && (fileFd = ::open("test.txt", O_RDONLY)))
    {
        ::fstat(fileFd, &stat_buf);

        auto count = stat_buf.st_size;
        while (count > 0)
        {
            const int BUF_SIZE = 8192;
            char buf[BUF_SIZE] = { 0 };

            auto toRead = count < BUF_SIZE ? count : BUF_SIZE;

            auto numRead = ::read(fileFd, buf, toRead);
            if (numRead == -1)
                throw Exception("Can't sent file");
            if (numRead == 0)
                break; // EOF

            std::string sendBuf(buf, numRead);
            auto numSent = this->socket_->send(connection.getFD(), sendBuf);
            if (numSent == -1)
                throw Exception("Can't sent file");
            if (numSent == 0)
                throw Exception("Can't sent file");

            count -= numSent;
        }

        ::close(fileFd);
    }
    else
    {
        throw Exception("Can't sent file, no such file");
    }
}
