#include <exception.hpp>


Exception::Exception(const std::string& error, const int code) : code_(code), error_(error)
{
    ;
}

const char* Exception::what() const noexcept
{
    return error_.c_str();
}

int Exception::getCode() const noexcept
{
    return code_;
}
