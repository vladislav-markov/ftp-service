#include <memory>
#include <iostream>

#include <ftp/server.hpp>
#include <tcp/socket.hpp>
#include <exception.hpp>


int main(const int argc, const char** argv)
{
    try
    {
        std::shared_ptr<ISocket> socket = std::make_shared<tcp::Socket>();

        auto ftpServer = ftp::Server(socket, "127.0.0.1", 8021);
        ftpServer.run();
    }
    catch (Exception& exception)
    {
        std::cerr << exception.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "Caught unhandling exception!" << std::endl;
    }

    return 0;
}
