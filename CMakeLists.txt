cmake_minimum_required(VERSION 3.5.1)

set(CMAKE_CXX_STANDARD 14) 
set(CMAKE_CXX_STANDARD_REQUIRED on)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")


project(ftp-server)

include_directories(include)

add_subdirectory(src)
add_subdirectory(src/ftp)
add_subdirectory(src/tcp)

enable_testing()

add_subdirectory(test)

add_executable(ftp-server main.cpp)

target_link_libraries(ftp-server module-exception module-ftp module-tcp)
