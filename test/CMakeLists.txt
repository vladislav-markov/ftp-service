set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED on)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")

find_package(GTest REQUIRED)
find_package(Threads REQUIRED)

add_executable(TestFtpServer  ftp-server-test.cpp)
add_executable(TestUnitSimple test-unit-simple.cpp)

target_link_libraries(TestFtpServer module-ftp module-exception ${GTEST_LIBRARIES} Threads::Threads gtest gmock)
target_link_libraries(TestUnitSimple ${GTEST_LIBRARIES} Threads::Threads gtest)

add_test(NAME TestFtpServer  COMMAND TestFtpServer)
add_test(NAME TestUnitSimple COMMAND TestUnitSimple)
