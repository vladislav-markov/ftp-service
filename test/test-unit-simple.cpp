#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <math.h>


double squareRoot(const double a)
{
    const auto b = ::sqrt(a);

    if (b != b)
        return -1.0;
    else
        return ::sqrt(a);
}

TEST (SquareRootTest, PositiveNos)
{ 
    ASSERT_EQ(6.0,  squareRoot(36.0));
    ASSERT_EQ(18.0, squareRoot(324.0));
    ASSERT_EQ(25.4, squareRoot(645.16));
    ASSERT_EQ(0.0,  squareRoot(0.0));
}
 
TEST (SquareRootTest, NegativeNos)
{
    ASSERT_EQ(-1.0, squareRoot(-15.0));
    ASSERT_EQ(-1.0, squareRoot(-0.2));
}

TEST (SimpleTest, TestBehavior)
{
    ASSERT_TRUE(true);
    ASSERT_EQ(1, 1);
    ASSERT_LT(1, 3);
    ASSERT_GT(6, 5);
    ASSERT_THAT(1.0, ::testing::Eq(1.0));
}


int main(int argc, char **argv)
{ 
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
