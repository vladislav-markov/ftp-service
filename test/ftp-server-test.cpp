#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <isocket.hpp>
#include <ftp/server.hpp>
#include <ftp/connection.hpp>

#include <arpa/inet.h>


class MockSocket : public ISocket
{
    public:
        MockSocket()  = default;
        ~MockSocket() = default;

        MOCK_METHOD2(setOpt,              void(const int level, const int opt));
        MOCK_METHOD0(setNonBlocking,      void(void));
        MOCK_METHOD1(setMaxConnections,   void(const int maxConnections));
        MOCK_METHOD2(bind,                void(const std::string& addr, const int port));
        MOCK_METHOD0(listen,              void(void));
        MOCK_METHOD1(close,               int(const int fd));
        MOCK_METHOD0(accept,              int(void));
        MOCK_METHOD2(recv,                std::string(const int fd, const int flag));
        MOCK_METHOD3(send,                ssize_t(const int fd, const std::string& str, const int flag));
        MOCK_METHOD0(checkNewConnection,  bool(void));
        MOCK_METHOD1(monitorFds,          int(struct timeval& timeout));
        MOCK_METHOD0(clearMonitoringFds,  void(void));
        MOCK_METHOD1(addMonitoringFd,     void(const int fd));
        MOCK_METHOD1(removeMonitoringFd,  void(const int fd));
        MOCK_METHOD1(isMonitoringFdReady, bool(const int fd));
};


class FtpServer: public ::testing::Test
{
    protected:
        static void SetUpTestCase()
        {
            fd = 5;

            socket = std::make_shared<MockSocket>();

            MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());

            EXPECT_CALL(*mockSocket, setOpt(SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT)).Times(1);
            EXPECT_CALL(*mockSocket, setNonBlocking()).Times(1);
            EXPECT_CALL(*mockSocket, setMaxConnections(10)).Times(1);
            EXPECT_CALL(*mockSocket, bind("127.0.0.1", 8021)).Times(1);

            server     = new ftp::Server(socket, "127.0.0.1", 8021);
            connection = new ftp::Connection(fd);
        }

        static void TearDownTestCase()
        {
            delete connection;
            connection = nullptr;

            delete server;
            server = nullptr;

            socket.reset();
        }

        static ftp::Server*             server;
        static ftp::Connection*         connection;
        static int                      fd;
        static std::shared_ptr<ISocket> socket;
};

ftp::Server*             FtpServer::server;
ftp::Connection*         FtpServer::connection;
int                      FtpServer::fd;
std::shared_ptr<ISocket> FtpServer::socket;


TEST(FtpServerCreating, NoThrowOnCreate)
{
    std::shared_ptr<ISocket> socket = std::make_shared<MockSocket>();

    MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());

    EXPECT_CALL(*mockSocket, setOpt(SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT)).Times(1);
    EXPECT_CALL(*mockSocket, setNonBlocking()).Times(1);
    EXPECT_CALL(*mockSocket, setMaxConnections(10)).Times(1);
    EXPECT_CALL(*mockSocket, bind("127.0.0.1", 8021)).Times(1);

    ASSERT_NO_THROW(ftp::Server(socket, "127.0.0.1", 8021));
}

TEST_F(FtpServer, HasConnectionSizeOfOneAfterConnectionAdded)
{
    server->addConnection(*connection);
    ASSERT_THAT(server->getConnectionNumbers(), ::testing::Eq(1));
}

TEST_F(FtpServer, HasConnectionSizeOfZeroAfterConnectionRemoved)
{
    MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());
    EXPECT_CALL(*mockSocket, close(fd)).Times(1);

    server->removeConnection(*connection);
    ASSERT_THAT(server->getConnectionNumbers(), ::testing::Eq(0));
}

TEST_F(FtpServer, HasNoActionsAfterCheckIfConnectionIsNotReady)
{
    auto connection = ftp::Connection(3);
    MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());

    EXPECT_CALL(*mockSocket, isMonitoringFdReady(connection.getFD())).Times(1).WillOnce(::testing::Return(false));
    EXPECT_CALL(*mockSocket, recv(connection.getFD(), ::testing::_)).Times(::testing::Exactly(0));
    EXPECT_CALL(*mockSocket, send(connection.getFD(), ::testing::_, ::testing::_)).Times(::testing::Exactly(0));

    server->processConnection(connection);
}

TEST_F(FtpServer, HasNoActionsAfterReceivedEmptyMessage)
{
    auto connection = ftp::Connection(3);
    MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());

    EXPECT_CALL(*mockSocket, isMonitoringFdReady(connection.getFD())).Times(1).WillOnce(::testing::Return(true));
    EXPECT_CALL(*mockSocket, recv(connection.getFD(), ::testing::_)).Times(1).WillOnce(::testing::Return(std::string()));
    EXPECT_CALL(*mockSocket, send(connection.getFD(), ::testing::_, ::testing::_)).Times(::testing::Exactly(0));

    server->processConnection(connection);
}

TEST_F(FtpServer, HasSendOnReceivedHelpMessage)
{
    auto connection = ftp::Connection(3);
    std::string buffer("AVILABLE COMMNADS:\nHELP\nQUIT\nRETR\n");
    MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());

    EXPECT_CALL(*mockSocket, isMonitoringFdReady(connection.getFD())).Times(1).WillOnce(::testing::Return(true));
    EXPECT_CALL(*mockSocket, recv(connection.getFD(), ::testing::_)).Times(1).WillOnce(::testing::Return(std::string("HELP")));
    EXPECT_CALL(*mockSocket, send(connection.getFD(), buffer, ::testing::_)).Times(1).WillOnce(::testing::Return(buffer.size()));

    server->processConnection(connection);
}

TEST_F(FtpServer, HasCloseOnReceivedQuitMessage)
{
    auto connection = ftp::Connection(3);
    MockSocket *mockSocket = dynamic_cast<MockSocket*>(socket.get());

    EXPECT_CALL(*mockSocket, isMonitoringFdReady(connection.getFD())).Times(1).WillOnce(::testing::Return(true));
    EXPECT_CALL(*mockSocket, recv(connection.getFD(), ::testing::_)).Times(1).WillOnce(::testing::Return(std::string("QUIT")));
    EXPECT_CALL(*mockSocket, close(connection.getFD())).Times(1).WillOnce(::testing::Return(0));

    server->addConnection(connection);
    server->processConnection(connection);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleMock(&argc, argv);

    return RUN_ALL_TESTS();
}
